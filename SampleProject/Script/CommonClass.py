﻿class common:
  #ocr method click
  def click_with_ocr(self,elementpath,elementtext):
    self.elementpath=elementpath
    self.elementtext=elementtext
    try:
      OCR.Recognize(elementpath).BlockByText(elementtext).Click()
    except:
      Log.Error("Element Not clicked Using OCR")
    
  #ocr get text
  def gettext_with_ocr(self,elementpath):
    self.elementpath=elementpath
    try:
      Obtainedstring = OCR.Recognize(elementpath).FullText
      return Obtainedstring
    except:
      Log.Error("Unable to get the text")
      return None
  
  #click with x,y coordinates function
  def click_coordinates(self,elementpath,xcor,ycor):
    try:
      self.elementpath=elementpath
      self.xcor=xcor
      self.ycor=ycor
      elementpath.Click(xcor,ycor)
      Log.Message('Element clicked with Xcoordinate range of:'+xcor+' and Ycoordinate range of:'+ycor+'')
    except:
      Log.Error("Element Not clicked")
  #click function
  def click(self,elementpath):
    try:
      self.elementpath=elementpath
      elementpath.Click()
      Log.Message("Element clicked")
    except:
      Log.Error("Element Not clicked")
  #exists function
  def VisibleOnScreen(self,elementpath):
      try:
        self.elementpath=elementpath
        if (elementpath.VisibleOnScreen):
          return True
        else:
          return False
      except:
        return False
  #exists function
  def exists(self,elementpath):
      try:
        self.elementpath=elementpath
        if (elementpath.Exists):
          return True
        else:
          return False
      except:
        return False
  #send keys function
  def Sendkeys(self,elementpath,Stringvalue):
    try:
      self.elementpath = elementpath
      self.Stringvalue = Stringvalue
      elementpath.Keys(Stringvalue)
      Log.Message("Input Entered in the text box")   
    except:
      Log.Error("Unable to enter the input")

  #double click function
  def doubleclick(self,elementpath):
    try:
      self.elementpath=elementpath
      elementpath.DblClick()
      Log.Message("Element double clicked")
    except:
      Log.Error("Unable to double click the current element")

  #close with testedapps function
  def close_testedapp(self,testedappname):
    try:
      self.testedappname=testedappname
      testedappname.Close()
      Log.Message("Current tested app closed successfully")
    except:
      Log.Error("Current tested app Not closed")
  
  #minimize the current app function
  def minimize(self,processname,windownname):
    try:
      self.processname=processname
      min = Sys.Process(processname).Window(windownname, "*", 1)
      min.Activate()
      min.Minimize()
    except:
      Log.Error("Unable to minimize the app")

  #teminate the current app function
  def terminate(self,processname):
    try:
      self.processname=processname
      Sys.Process(processname).Terminate()
      Log.Message("Current app terminated successfully")
    except:
      Log.Error("Unable to terminate the app")  

  #close the current app function : processname
  def closewithprocessname(self,processname):
    try:
      self.processname=processname
      Sys.Process(processname).Close()
      if processname.Exists:
            Log.Warning(p.ProcessName + "Some problem with close the application, so terminating")
            # Terminates the process
            processname.Terminate()
      else:
        Log.Message("Current app closed successfully")
    except:
      Log.Error("Unable to close current app Process") 
      
  #close the current app child windows function : processname
  def close_childwindow_withprocessname(self,processname,childprocessname):
    try:
      self.processname=processname
      self.childprocessname=childprocessname
      childwindow = Sys.FindChild(processname, childprocessname)
      #check all the cjild windows
      while childwindow.Exists:
        childwindow.Close()
        # Checks whether the process is closed
        if childwindow.Exists:
          Log.Warning(p.ProcessName + "Some problem with close the application, so terminating")
          # Terminates the process
          childwindow.Terminate()
        else:
          Log.Message("Current child window closed successfully")
    except:
      Log.Error("Unable to close current child Process") 
