﻿from CommonClass import common

class LoginPage():
  global commonclassobj
  global LoginpageObj
  commonclassobj = common()
  
  #Login page : Usename and Pass
  def LoginWindowverification(self):
    if (commonclassobj.VisibleOnScreen(NameMapping.Sys.OfficePractium.Mainmenu.Loginwindow.Loginpage)):
        Log.Message("Login page enabled")
        commonclassobj.Sendkeys(Aliases.OfficePractium.Mainmenu.Loginwindow.Loginpage.InputUsername.UserTextEdit,"DD9")
        commonclassobj.Sendkeys(Aliases.OfficePractium.Mainmenu.Loginwindow.Loginpage.InputPassword.PasswordEdit,"connexin1")
        commonclassobj.click(Aliases.OfficePractium.Mainmenu.Loginwindow.Loginpage.LoginButton)
        aqUtils.Delay(3000)
        self.LoginErrorPopVerification()
    else:
      Log.Message("User Already Logged in For Today")
     
    #Login Page : Error Popup Handling 
  def LoginErrorPopVerification(self):  
    if (commonclassobj.exists(NameMapping.Sys.OfficePractium.ErrorPopup)):
      Log.Warning("Login Page Error Popup Appeared")
      commonclassobj.click(NameMapping.Sys.OfficePractium.ErrorPopup.ErrorPopupOkButton)
    else:
      Log.Message("Login Page Error Popup Not Appeared")
