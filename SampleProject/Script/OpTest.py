﻿from CommonClass import common
from OP_LoginPage import LoginPage


def optest():
  LoginPageobj = LoginPage()
  commonclassobj = common()
  #WshShell.Run("D:\\OfficePracticum\\OP\\op.exe")
  TestedApps.op.Run()
  aqUtils.Delay(5000)
  LoginPageobj.LoginWindowverification()
  #LoginPageobj.LoginWindowverification()
  obtainedtext = commonclassobj.gettext_with_ocr(NameMapping.Sys.OfficePractium.Mainmenu.TopMenuBar.Topmenubarchildone.Topmenubarchildtwo.TopmenubarOptions)
  Log.Message(obtainedtext)
  commonclassobj.click_with_ocr(NameMapping.Sys.OfficePractium.Mainmenu.TopMenuBar.Topmenubarchildone.Topmenubarchildtwo.TopmenubarOptions,"Patient")
  
  #Send input to the patient search box
  commonclassobj.Sendkeys(NameMapping.Sys.OfficePractium.Mainmenu.PatientChartWindow.
  PatientChartWindowChild.PatientChartChildwinTwo.PatientChartWindowChildThree.PatientSearchBar
  .PatientSearchBarInputBar.PatientSearchBarInputBarEdit,"Raj,Mohan")
  
  #click with x,y cords
  commonclassobj.click_coordinates(NameMapping.Sys.OfficePractium.Mainmenu.PatientChartWindow.
  PatientChartWindowChild.PatientChartChildwinTwo.PatientChartWindowChildThree.PatientSearchBar
  .PatientSearchBarInputBar,149,21)
  aqUtils.Delay(5000)
  nopatientstext = commonclassobj.gettext_with_ocr(NameMapping.Sys.OfficePractium.ErrorPopup.NoPatientResultPopup.No_matching_patients)
  Log.Message(nopatientstext)
  commonclassobj.click(NameMapping.Sys.OfficePractium.ErrorPopup.NoPatientResultPopup.Noresultsokbutton.Clickokbutton)
  aqUtils.Delay(2000)
  commonclassobj.click(NameMapping.Sys.OfficePractium.AddNewPatientButton.NewPatientAddWindow.ButtonAction.NewPatientAddButton)
  commonclassobj.close_testedapp(TestedApps.op)
